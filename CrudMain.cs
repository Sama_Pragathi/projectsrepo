﻿using System;
using System.Data.SqlClient;


namespace Crudoperations
{
    class CrudMain
    {
        static void Main()
        {
            Database connection = new Database();
            int choose;
            Console.WriteLine("press * to show options");
            char options = '*';
            if (options == '*')
            {
                do
                {
                    Console.WriteLine("choose option");
                    Console.WriteLine("1.add students  into student table");
                    Console.WriteLine("2.Update name and age based on student id");
                    Console.WriteLine("3.retrieve all students");
                    Console.WriteLine("4.Retrieve student details based on student id");
                    Console.WriteLine("5.delete student based on student id");
                    choose = Convert.ToInt32(Console.ReadLine());
                    switch (choose)
                    {
                        case 1:
                            AddStudent(connection);
                            break;
                        case 2:
                            Update(connection);
                            break;
                        case 3:
                            RetrieveAll(connection);
                            break;
                        case 4:
                            RetrieveBasedOnId(connection);
                            break;
                        case 5:
                            Detele(connection);
                            break;
                    }

                } while (choose <= 5);
            }
            
        }
        /// <summary>
        /// Functions implementation
        /// </summary>
        /// <param name="connection"></param>
        public static void RetrieveAll(Database connection)
        {
            connection.openConnectin();
            SqlCommand cmd = new SqlCommand("select * from GetAllStudents()", connection.GetConnection());//retrive all students using getallstudents function
            var dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                Console.WriteLine($"{dataReader.GetValue(0)}{dataReader.GetValue(1)}{dataReader.GetValue(2)}");
            }
            dataReader.Close();
            connection.closeConnection();
        }
        public static void RetrieveBasedOnId(Database connection)
        {
            connection.openConnectin();
            Console.WriteLine("enter studentid to retrieve data:");
            int studentId = Convert.ToInt32(Console.ReadLine());
            // SqlCommand cmd = new SqlCommand("select * from studentDetails where studentId='" + studentId + "'", connection.GetConnection());
            SqlCommand cmd = new SqlCommand("select * from GetStudents(@id)", connection.GetConnection());//Function retrieve based on id
            cmd.Parameters.AddWithValue("@id", studentId);
            var dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                Console.WriteLine($"{dataReader.GetValue(0)}{dataReader.GetValue(1)}{dataReader.GetValue(2)}");
            }
            dataReader.Close();
            connection.closeConnection();
        }
        public static void AddStudent(Database connection)
        {
            connection.openConnectin();
            Console.WriteLine("enter student id:");
            int studentId = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter student name:");
            string studentName = Console.ReadLine();
            Console.WriteLine("enter age of student:");
            int studentAge = Convert.ToInt32(Console.ReadLine());
            SqlCommand cmd = new SqlCommand("insert into studentDetails values('" + studentId + "','" + studentName + "','" + studentAge+ "')", connection.GetConnection());
            cmd.ExecuteNonQuery();
            connection.closeConnection();
            Console.WriteLine();

        }
        public static void Update(Database connection)
        {
            connection.openConnectin();
            Console.WriteLine("enter student name:");
            string studentName = Console.ReadLine();
            Console.WriteLine("enter student age:");
            int studentAge = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter student id which you want to update");
            int studentId = Convert.ToInt32(Console.ReadLine());
            SqlCommand cmd = new SqlCommand("update studentDetails set studetName='"+studentName+"' studentAge='"+studentAge+"' where studentId='"+studentId+"'", connection.GetConnection());
            cmd.ExecuteNonQuery();
            connection.closeConnection();
        }
        public static void Detele(Database connection)
        {
            connection.openConnectin();
            Console.WriteLine("enter student id to delete record of particular student:");
            int studentId = Convert.ToInt32(Console.ReadLine());
            SqlCommand cmd = new SqlCommand("delete from studentDetails where studentId='"+studentId+"'",connection.GetConnection());
            cmd.ExecuteNonQuery();
            connection.closeConnection();
        }
    }
}
