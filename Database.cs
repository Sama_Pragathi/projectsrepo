﻿using System;
using System.Configuration;
using System.Data.SqlClient;


namespace Crudoperations
{
    class Database
    {
        //get connection strng from app.configure
        private static string conString = ConfigurationManager.ConnectionStrings["pragathi"].ConnectionString;
        //sql connection
        private SqlConnection con = new SqlConnection(conString);
        public SqlConnection GetConnection()
        {
            return con;
        }
        public void openConnectin()
        {
            if (con.State == System.Data.ConnectionState.Closed)
            {
                con.Open();
            }
        }
        public void closeConnection()
        {
            if (con.State == System.Data.ConnectionState.Open)
            {
                con.Close();
            }
        }
    }
}
